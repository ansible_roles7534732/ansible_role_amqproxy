Amqproxy role
=========

Installs amqproxy

Requirements
------------

—

Role Variables
--------------

See defaults/main/*

Dependencies
------------

—

Example Playbook
----------------

```
---
- name: Install amqproxy
  hosts: rabbitmq
  become: true
  gather_facts: true
  roles:
    - amqproxy
      tags:
        - role_amqproxy
```

License
-------

MIT

Author Information
------------------

n98gt56ti@gmail.com
